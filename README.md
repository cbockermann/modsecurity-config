The `modsecurity-config` Package
================================

This packages does simply provide a directory layout for 
ModSecurity setups. It follows the layout of the [ModSecurity Handbook](https://www.feistyduck.com)
as close as possible.


What this package provides
--------------------------

The objectives of this package are as follows:

  1. Provide a simple way of setting up a ModSecurity configuration
     as quickly as possible.

  2. Decouple the management of rules from the bare layout of a
     ModSecurity based WAF system.

  3. Give guidance to a set of *best practices* for managing a
     ModSecurity system and handling the logs it produces.


What this package *does not* provide
------------------------------------

This package will *not* provide a generic rule set nor anything
that will automatically upgrade your rules when installing the
next version of this package.

Instead, we want to give a guidance on where to place rules within
the directory layout provided by this package and how to create
rules.

