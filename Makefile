# $Id$
#
# This Makefile contains all instructions to create RPM and DEB packages
# for the AuditConsole
#
#
VERSION=0.3
REVISION=0
NAME=modsecurity-config
BUILD=.build_tmp
DIST=jwall
DEB_FILE=${NAME}-${VERSION}-${REVISION}.deb
RPM_FILE=${NAME}-${VERSION}-${REVISION}.noarch.rpm
RELEASE_DIR=releases
RPMBUILD=$(PWD)/.rpmbuild
ARCH=noarch

all:  deb


pre-package:
	@echo "Preparing package build in ${BUILD}"
	@mkdir -p ${RELEASE_DIR}
	@mkdir -p ${BUILD}
	@mkdir -p dist/opt
	@cp -a dist/opt ${BUILD}/

rpm:    pre-package
	@echo "Creating RPM package..."
	@mkdir -p ${RPMBUILD}
	@mkdir -p ${RPMBUILD}/tmp
	@mkdir -p ${RPMBUILD}/RPMS
	@mkdir -p ${RPMBUILD}/RPMS/${ARCH}
	@mkdir -p ${RPMBUILD}/BUILD
	@mkdir -p ${RPMBUILD}/SRPMS
	@rm -rf ${RPMBUILD}/BUILD
	@mkdir -p ${RPMBUILD}/BUILD
	@cp -a dist/opt ${RPMBUILD}/BUILD
	@rm -rf ${RPMBUILD}/BUILD/DEBIAN
	@mkdir -p ${RPMBUILD}/SPECS
	@cp -a dist/modsecurity-config.spec ${RPMBUILD}/SPECS
	@find .rpmbuild/BUILD -type f | sed -e s/^\.rpmbuild\\/BUILD// | grep -v DEBIAN > ${RPMBUILD}/tmp/rpmfiles.list
	@rpmbuild --target noarch --sign --define '_topdir ${RPMBUILD}' --define '_version ${VERSION}' --define '_revision ${REVISION}' -bb ${RPMBUILD}/SPECS/modsecurity-config.spec --buildroot ${RPMBUILD}/BUILD/
	@cp ${RPMBUILD}/RPMS/${ARCH}/${RPM_FILE} ${RELEASE_DIR}
	@md5sum ${RELEASE_DIR}/${RPM_FILE} > ${RELEASE_DIR}/${RPM_FILE}.md5

release-rpm:
	@echo "Releasing RPM package to download.jwall.org..."
	@mkdir -p /var/www/download.jwall.org/htdocs/yum/${DIST}/noarch
	@cp ${RELEASE_DIR}/${RPM_FILE} /var/www/download.jwall.org/htdocs/yum/${DIST}/noarch/
	@createrepo /var/www/download.jwall.org/htdocs/yum/${DIST}/


deb:	pre-package
	@echo "Building Debian package..."
	@mkdir -p ${BUILD}/DEBIAN
	@cp dist/DEBIAN/* ${BUILD}/DEBIAN/
	@cat dist/DEBIAN/control | sed -e 's/Version:.*/Version: ${VERSION}-${REVISION}/' > ${BUILD}/DEBIAN/control
	@chmod 755 ${BUILD}/DEBIAN/p*
	@cd ${BUILD} && find opt -type f -exec md5sum {} \; > DEBIAN/md5sums && cd ..
	@dpkg -b ${BUILD} ${RELEASE_DIR}/${DEB_FILE}
	@md5sum ${RELEASE_DIR}/${DEB_FILE} > ${RELEASE_DIR}/${DEB_FILE}.md5
	@rm -rf ${BUILD}
	@echo "Signing .deb file..."
	@debsigs --sign=origin --default-key=C5C3953C ${RELEASE_DIR}/${DEB_FILE}

release-deb:
	@echo "Releasing Debian package to download.jwall.org..."
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian includedeb ${DIST} ${RELEASE_DIR}/${DEB_FILE}


unrelease-deb:
	@echo "Un-releasing Debian package from download.jwall.org"
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian remove ${DIST} ${NAME}



clean:	clean-rpm clean-deb clean-zip
	@echo "Removing release directory..."
	@rm -rf ${RELEASE_DIR}

clean-rpm:
	@echo "Removing RPM package files..."
	@rm -rf ${RPMBUILD}
	@rm -f ${RPM_FILE}

clean-deb:
	@echo "Removing Debian package files..."
	@rm -rf ${BUILD}
	@rm -f ${DEB_FILE}

clean-zip:
	@rm -rf ${BUILD}/zip
	@rm -f ${NAME}-${VERSION}.zip

sign-releases:
	sh src/main/assembly/sign-releases.sh ./${RELEASE_DIR}

upload:
	cd ${RELEASE_DIR} && scp * chris@jwall.org:/var/www/download.jwall.org/upload/


release: clean war deb rpm

mvn-release:
	@echo "Running maven-release..."
	mvn -Darguments='-Dmaven.test.skip=true' -DskipTests=true -DdryRun=false release:clean release:prepare


reset:
	@echo "Resetting all buildNumber.properties"
	@find . -name 'buildNumber.properties' -exec hg revert {} \;
	@echo "Removing all buildNumber.properties.orig files"
	@find . -name 'buildNumber.properties.orig' -exec rm {} \;

